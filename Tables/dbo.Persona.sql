CREATE TABLE [dbo].[Persona]
(
[id_persona] [int] NOT NULL,
[nombre] [char] (50) COLLATE Modern_Spanish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Persona] ADD CONSTRAINT [PK__Persona__228148B085B173D4] PRIMARY KEY CLUSTERED  ([id_persona]) ON [PRIMARY]
GO
